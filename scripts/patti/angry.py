#!/usr/bin/env python

from state import State

class Angry(State):
    '''
    This class is for angry state
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.name = 'Angry'
	#those are settings for robot when it is bored
        self.RGBsettings = 'RGB colour: (255,0,0) red '
	self.coverSettings = 'Open and close fast'
	self.movementSettings = 'Going around fast'

    '''
    @see state
    '''
    def getName(self):
        return self.name + ' X('
    

'''
Use this with debug
'''
if __name__ == '__main__':
    angryState = Angry()
    print('Hey! I am ' + angryState.__str__() + ' X(')
