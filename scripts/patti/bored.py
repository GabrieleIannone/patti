#!/usr/bin/env python

from state import State

class Bored(State):
    '''
    This class is for bored state
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.name = 'Bored'
	#those are settings for robot when it is bored
        self.RGBsettings = 'RGB colour: (143,0,255) purple '
	self.coverSettings = 'Closed'
	self.movementSettings = 'Going slowly'
 
    '''
    @see state
    '''
    def getName(self):
        return self.name + ' -_-'
    
'''
Use this with debug
'''
if __name__ == '__main__':
    boredState = Bored()
    print('Hey! I am ' + boredState.__str__() + ' -_-')
