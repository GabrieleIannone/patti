#!/usr/bin/env python

from patti import Patti
from random import randint
#from interfaceROS import InterfaceROS
import interfaceROS
import patti
import time

'''
This class is the controller for patti core and it is the one which decide what to do
'''
class Controller(object):

    '''
    In the builder i define threshold for interaction and emptiness and set up patti and interfaceROS istance
    '''
    def __init__(self, interfaceROS):
	self.interactionThreshold = 500
	self.emptinessThreshold = 250
	self.pattiIstance = Patti()
	self.interfaceROS = interfaceROS 
	self.emptinessValueString = "Emptiness value: "
	self.interactionValueString = "Interaction value: "

    '''
    This method is used to get a specific sensor value
    
    @param: sensorinteraction
		two interaction sensor values
    @param: sensorEmptiness
		three emptiness sensor values 
    '''	
    def getSensorValue(self, sensorInteraction0, sensorInteraction1, sensorEmptiness0, sensorEmptiness1, sensorEmptiness2): 
	#elaborate sensorsvalues
	(self.finalInteractionSensorValue, self.finalEmptinessSensorValue) = self.elaborateSensorValues(sensorInteraction0, sensorInteraction1, sensorEmptiness0, sensorEmptiness1, sensorEmptiness2)
	
	#call the changeState function

	print
	print (self.emptinessValueString+ str(self.finalEmptinessSensorValue))
	print (self.interactionValueString+ str(self.finalInteractionSensorValue))
	#set current state in patti istance and get it
	self.currentState = self.pattiIstance.setCurrentState(self.finalEmptinessSensorValue, self.finalInteractionSensorValue)
	#call the setState for interfaceROS istance 
	self.interfaceROS.setState(self.currentState)

    '''
    This method is used to set current state due to sensors value and change robot settings
    
    @param: finalEmptinessSensorValue
		 is the emptyness sensor value
    @param: finalInteractionSensorValue
		 is the interaction sensor value
    '''
    def changeState (self, finalEmptinessSensorValue, finalInteractionSensorValue):
        pattiInstance.setCurrentState(finalEmptinessSensorValue, finalInteractionSensorValue)
	interfaceROS.setState(pattiInstance.currentState)

    '''
    Use this function in order to get the interaction and emptiness level

    @param: sensorinteraction
		two interaction sensor values
    @param: sensorEmptiness
		three emptiness sensor values 

    @return: finalInteractionSensorValue
		final intercative level
	     finalEmptinessSensorValue
		final emptiness level
    '''	
    def elaborateSensorValues(self, sensorInteraction0, sensorInteraction1, sensorEmptiness0, sensorEmptiness1, sensorEmptiness2):
	sensorinteraction = []
	sensorinteraction.append(sensorInteraction0)
	sensorinteraction.append(sensorInteraction1)
	#Here I print the interaction values controller received in order to debug
	for i in range (0,2): 
		print "Received value interaction " + str(i) + ": " + str(sensorinteraction[i])
	
	sensorEmptiness = []
	sensorEmptiness.append(sensorEmptiness0)
	sensorEmptiness.append(sensorEmptiness1)
	sensorEmptiness.append(sensorEmptiness2)
	#Here I print the interaction values controller received in order to debug
	for i in range (3): 
		print ("Received value emptiness " + str(i) + ": " + str(sensorEmptiness[i]))

	#Here I get final interaction sensor value: if the interaction is higher than an edge then there is interaction else finalInteractionSensorValue = 0
	if sensorinteraction[0] >= self.interactionThreshold:
		if sensorinteraction[1] >= self.interactionThreshold:
			finalInteractionSensorValue = 2
		else:
			finalInteractionSensorValue = 0
	else:
		finalInteractionSensorValue = 0
	
	#I got 3 level here for emptiness 0, 1, 2
	#if the lower sensor tell us patti is empty finalEmptinessSensorValue = 0
	#if the second sensor tell us patti is middle empty finalEmptinessSensorValue = 1
	#if the third sensor tell us patti is full finalEmptinessSensorValue = 2
	#if the lower sensor and the third tell us that there is a long object that avoid the second sensor then finalEmptinessSensorValue = 1
	if sensorEmptiness[0] >= self.emptinessThreshold:
		if sensorEmptiness[1] >= self.emptinessThreshold:
			if sensorEmptiness[2] >= self.emptinessThreshold:
				finalEmptinessSensorValue = 2
			else :
				finalEmptinessSensorValue = 1
		else :
			finalEmptinessSensorValue = 1
		if sensorEmptiness[2] >= self.emptinessThreshold:
			finalEmptinessSensorValue = 1
	else:
		finalEmptinessSensorValue = 0

	return (finalInteractionSensorValue, finalEmptinessSensorValue)

'''
Used for debug if all system work.
Try 10 repetition of random sensors values
'''            
if __name__ == '__main__':
    contri = Controller()
    putti = Patti()
    for repetition in range (0,10):
	s1 = randint(0,2)
	print (s1)
	s2 = randint(0,1)*2
	print (s2)
	self.changeState(s1, s2, putti)
	time.sleep(2)
	print ('')
    

