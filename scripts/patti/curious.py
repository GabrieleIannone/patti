#!/usr/bin/env python

from state import State

class Curious(State):
    '''
    This class is for curious state
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.name = 'Curious'
	#those are settings for robot when it is curious
        self.RGBsettings = 'RGB colour: (0,255,0) green '
	self.coverSettings = 'Open'
	self.movementSettings = 'Stationary and spinning around'
 
    '''
    @see state
    '''
    def getName(self):
        return self.name + ' O.O'
    

'''
Use this with debug
'''
if __name__ == '__main__':
    curiousState = Curious()
    print('Hey! I am ' + curiousState.__str__() + ' O.O')
