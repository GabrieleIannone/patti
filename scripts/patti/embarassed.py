#!/usr/bin/env python

from state import State

class Embarassed(State):
    '''
    This class is for Embarassed state 	
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.name = 'Embarassed'
	#those are settings for robot when it is embarassed
        self.RGBsettings = 'RGB colour: (255,153,0) orange '
	self.coverSettings = 'Open'
	self.movementSettings = 'Stationary and swinging'
 
    '''
    @see state
    '''
    def getName(self):
        return self.name + ' >///<'
    

'''
Use this for debugging
'''
if __name__ == '__main__':
    embarassedState = Embarassed()
    print('Hey! I am ' + embarassedState.__str__() + ' >///<')
