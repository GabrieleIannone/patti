#!/usr/bin/env python

from state import State

class Happy(State):
    '''
    This class is for Happy state
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.name = 'Happy'
	#those are settings for robot when it is happy
        self.RGBsettings = 'RGB colour: (255,255,0) yellow '
	self.coverSettings = 'Open and wagging'
	self.movementSettings = 'Stationary'

    '''
    @see state
    '''
    def getName(self):
        return self.name + ' :D'
    

'''
Use this for debug
'''
if __name__ == '__main__':
    happyState = Happy()
    print('Hey! I am ' + happyState.__str__() + ' :D')
