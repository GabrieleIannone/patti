#!/usr/bin/env python

import state
import robot
from controller import Controller


'''

'''
class InterfaceROS(object):

	def __init__ (self, pattiCore):
		self.pattiCore = pattiCore
		self.controllerPatti = Controller(self)

	'''
	This interface is the way to make Patti core interact with the ros core
	'''
	def sendSensorValues(self, sensorInteraction0, sensorInteraction1, sensorEmptiness0, sensorEmptiness1, sensorEmptiness2):
		self.controllerPatti.getSensorValue(sensorInteraction0, sensorInteraction1, sensorEmptiness0, sensorEmptiness1, sensorEmptiness2)
	
	'''
	
	'''
	def setState(self, state):
		self.currentState = state
		self.getRobotSettings(self.currentState)
	'''
	
	'''
	def getRobotSettings (self, currentState):
		self.pattiCore.sendState(currentState.RGBsettings, currentState.coverSettings, currentState.movementSettings)
	
	'''

	'''
	def sendState(RGBsettings, coverSettings, movementSettings):
		pass
		


