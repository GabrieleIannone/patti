#!/usr/bin/env python
import rospy
import RPi.GPIO as GPIO
from time import sleep
from std_msgs.msg import String
from triskar_msgs.msg import Proximity
from geometry_msgs.msg import Twist
from patti_project.msg import EmotionalStateSettings

'''
This is the class used to manage the robot movement in a free space
'''
class Movement(object):

	thresholdObstacle = 300

	'''
	This is the builder where I set up listeners
	'''
	def movement(self):
		#will initialize this node listener
    		rospy.init_node('pattiCore', anonymous=True)
    		#here I subscribe to sensorValuesTopic, type of message is SensorValues and getValues is the function I use 
    		#after receive a message
    		rospy.Subscriber('emotialStateSettingsTopic', EmotionalStateSettings, self.setState)
		rospy.Subscriber('/proximity', Proximity, self.move)
    		# rospy spin keeps python to exit
    		rospy.spin()

	'''
	This is the main function of the class and manage the movement going to evaluate obstacle presence and the robot motion
	'''
	def setState(self, msg):
		rospy.loginfo ("RGB: " + msg.RGB)
		rospy.loginfo ("Cover: " + msg.cover)
		rospy.loginfo ("Movement: " + msg.movement)
		#(obstacleLeft, obstacleRight) = valueObstsacles(emotionalStateSettings)
		#spin(obstacleLeft, obstacleRight)
	
	'''
	This function is the function which handle movement
	@param msg
		is the message from sonar sensors
	'''
	def move (self, msg):
		#this set the manes to board mode. which just names the pins according to the numbers written on the board
		GPIO.setmode(GPIO.BOARD)
    	#this is the output to send pwn signal on
		GPIO.setup(03, GPIO.OUT)
		#setup pwn on pin 3 @50Hz
		self.pwn = GPIO.PWM (03,50)
		#get obstacle information
		(obstacleLeft, obstacleRight) = self.valueObstsacles(msg)
		#set a publisher for cmd_val topic which is the topic for motor
		pub = rospy.Publisher('cmd_vel', Twist, queue_size = 5)
		#create a new message type Twist (standard for ros)
		cmd_msg = Twist()
		#move the cover
		
		#values obstacles and then move
		if not obstacleRight and not obstacleLeft:
			self.go(cmd_msg)
		elif not obstacleLeft:
			self.turnLeft(cmd_msg)
		elif not obstacleRight:
			self.turnRight(cmd_msg)
		else:
			self.stopMove(cmd_msg)
		#move the cover
		#self.setServoAngle(0, self.pwn)
		#self.setServoAngle(90, self.pwn)
		pub.publish(cmd_msg)
		print (cmd_msg)

	'''
	Use this function in order to value if there is an obstacle 
	@return obstacleLeft = TRUE if there is an obstacle on the left
		obstacleRight = TRUE if there is an obstacle on the right
	'''
	def valueObstsacles(self, msg):
		#check right sonar
		#if I found something before threshold value then there is an obstacle
                                
		if (msg.range[4] > self.thresholdObstacle):
			obstacleRight = False
		else:
			obstacleRight = True
		#check left sensor
		#if I found something before threshold value then there is an obstacle
		if (msg.range[5] > self.thresholdObstacle):
			obstacleLeft = False
		else: 
			obstacleLeft = True
		rospy.loginfo (msg.range[5])
		print (obstacleRight)
		rospy.loginfo (msg.range[4])
		print (obstacleLeft)
		return (obstacleLeft, obstacleRight)

	'''
	Use this function for going and moving forward
	@param msg
		the message in which I save speed values
	'''
	def go(self, msg):
		msg.linear.x = 0.4
		msg.angular.z = 0
	
	'''
	This is the movement state function forstop moving and turn 180 degree
	@param msg
		the message in which I save speed values
	'''
	def stopMove(self, msg):
		msg.linear.x = 0.0
		msg.linear.y = 0.0
		msg.angular.z = 0.4

	'''
	This is the function for turning left
	@param msg
		the message in which I save speed values
	'''
	def turnLeft(self, msg):
		msg.angular.z = 0.4

	'''
	This is the function for turning right
	@param msg
		the message in which I save speed values
	'''
	def turnRight(self, msg):
		msg.angular.z = -0.4

	'''
	This is the function for setting servo motors angle and move it
	@param angle
		is the angle of which you want to move the servo
	'''
	def setServoAngle(self, angle, pwn):
		pwnTest = pwn
		if angle == 0: 
                                        pwnTest.start(angle/18 +2)
                                        sleep(1)
                                        pwnTest.stop(7.5)
                else:
                                        pwnTest.start(angle/18+2)
                                        sleep(1)
                                        pwnTest.stop(2.5)
                GPIO.cleanup()
                                

if __name__ == '__main__':
    movement = Movement()
    movement.movement()
