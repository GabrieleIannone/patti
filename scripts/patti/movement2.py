#!/usr/bin/env python
import rospy
import RPi.GPIO as GPIO
from time import sleep
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from patti_project.msg import EmotionalStateSettings

'''
This is the class used to manage the robot movement in a free space
'''
class Movement(object):

	thresholdObstacle = 200	

	'''
	This is the builder where I set up listeners
	'''
	def movement(self):
		#will initialize this node listener
    		rospy.init_node('pattiCore', anonymous=True)
    		#here I subscribe to sensorValuesTopic, type of message is SensorValues and getValues is the function I use 
    		#after receive a message
    		rospy.Subscriber('emotialStateSettingsTopic', EmotionalStateSettings, self.setState)
		#rospy.Subscriber('/proximity', Proximity, self.move)
    		# rospy spin keeps python to exit 
    		rospy.spin()

	'''
	This is the main function of the class and manage the movement going to evaluate obstacle presence and the robot motion
	'''
	def setState(self, msg):
		rospy.loginfo ("RGB: " + msg.RGB)
		rospy.loginfo ("Cover: " + msg.cover)
		rospy.loginfo ("Movement: " + msg.movement)
		#(obstacleLeft, obstacleRight) = valueObstsacles(emotionalStateSettings)
		#spin(obstacleLeft, obstacleRight)
	
	'''
	This function is the function which handle movement
	@param msg
		is the message from sonar sensors
	'''
	def move (self, msg):
		#this set the manes to board mode. which just names the pins according to the numbers written on the board
		GPIO.setmode(GPIO.BOARD)
		#this is the output to send pwn signal on
		GPIO.setup(03, GPIO.OUT)
		#setup pwn on pin 3 @50Hz
		pwn = GPIO.PWM (03,50)
		#get obstacle information
		(obstacleLeft, obstacleRight) = self.valueObstsacles(msg)
		rospy.loginfo ("Mi sto muovendo!")
		#set a publisher for cmd_val topic which is the topic for motor
		pub = rospy.Publisher('cmd_vel', Twist, queue_size = 5)
		#create a new message type Twist (standard for ros)
		cmdMsg = Twist()
		#move the cover
		self.setServoAngle(90)
		#values obstacles and then move
		if ((!obstacleRight) and (!obstacleLeft):
			cmdMsg = self.go(cmdMsg)
		elif (!obstacleLeft):
			cmdMsg = self.turnLeft(cmdMsg)
		elif (!obstacleRight):
			cmdMsg = self.turnRight(cmdMsg)
		elif ((obstacleRight) and (obstacleLeft):
			cmdMsg = self.stopMove(cmdMsg)
		#move the cover
		self.setServoAngle(0)
		pub.Publish(cmdMsg)
		cmdMsg.linear.x = 0.5

	'''
	Use this function in order to value if there is an obstacle 
	@return obstacleLeft = TRUE if there is an obstacle on the left
		obstacleRight = TRUE if there is an obstacle on the right
	'''
	def valueObstsacles(msg):
		#check right sonar
		#if I found something before threshold value then there is an obstacle
		if (msg.range[1] > thresholdObstacle):
			obstacleRight = False
		else:
			obstacleRight = True
		#check left sensor
		#if I found something before threshold value then there is an obstacle
		if (msg.range[5] > thresholdObstacle):
			obstacleLeft = False
		else: 
			obstacleLeft = True
		rospy.loginfo ("Sensor1 " + msg.range[5])
		rospy.loginfo ("Sensor2: " + msg.range[1])
		return (obstacleLeft, obstacleRight)

	'''
	Use this function for going and moving forward
	@param msg
		the message in which I save speed values
	'''
	def go(self, msg):
		msg.linear.x = 0.5
	
	'''
	This is the movement state function forstop moving and turn 180 degree
	@param msg
		the message in which I save speed values
	'''
	def stopMove(self, msg):
		msg.linear.x = 0.0
		msg.linear.y = 0.0
		msg.linear.z = 0.0
		msg.angular.z = 8.0

	'''
	This is the function for turning left
	@param msg
		the message in which I save speed values
	'''
	def turnLeft(self, msg):
		msg.angular.z = 4.0

	'''
	This is the function for turning right
	@param msg
		the message in which I save speed values
	'''
	def turnRight(self, msg):
		msg.angular.z = -4.0

	'''
	This is the function for setting servo motors angle and move it
	@param angle
		is the angle of which you want to move the servo
	'''
	def setServoAngle(angle):
		duty = angle/18 +2
		#set the pin as output
		GPIO.output(03, True)
		pwm.ChangeDutyCicle(duty)
		sleep(1)
		GPIO.output (03,False)
		pwm.ChangeDutyCycle(0)	

if __name__ == '__main__':
    movement = Movement()
    movement.movement()
