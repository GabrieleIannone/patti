#!/usr/bin/env python3

import json
import sys
import importlib
import state

'''
This class is the Patti core and is used as a model for all system
'''
class Patti(object):

    '''
    This is the builder when I define the path for json file and the name of json file
    '''
    def __init__(self):
	self.path = '/home/airlab/ros/src/Patti/scripts/patti/'
	self.transiotionFileNameJson = 'transition.txt'

    '''
    Use this method in order to find state class
    '''
    def classForName(self, module_name, class_name):
    	# load the module, will raise ImportError if module cannot be loaded
    	stateModule = importlib.import_module(module_name)
    	# get the class, will raise AttributeError if class cannot be found
    	stateClass = getattr(stateModule, class_name)
    	return stateClass		

    '''
    This method will set the current state matching sensors values with his jason file 

    @return currentState
		patti current emotional state
    '''
    
    def setCurrentState(self, sensor1value, sensor2value):
	#open json file
	with open(self.path + self.transiotionFileNameJson) as json_file:
		data = json.load(json_file)
		#read json file 
		for t in data['transition']:
    			'''
			Those prints are used for debugging: read in the right way json file
			print ('State: ' + t['state'])
			print ('Module: ' +t['module'])
			print ('Empty sensor value: ' + t['s1'])
			print ('Interaction sensor value: ' + t['s2'])
    			'''
			#here I match json file values with sensors value and choose the currentstate
			if int(t['s1']) == sensor1value and int(t['s2']) == sensor2value :
				stateClass = self.classForName(t['module'],t['state'])
				currentState = stateClass()
				#this print will print the current state name
				print (currentState.getName())
				return currentState


'''
used for debug
'''
if __name__ == '__main__':
    putti = Patti()
    putti.setCurrentState(1, 0)  
