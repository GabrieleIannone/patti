#!/usr/bin/env python
import rospy
from patti_project.msg import SensorValues
from interfaceROS import InterfaceROS
from patti_project.msg import EmotionalStateSettings


class PattiCore(object):
    '''
    This is the costructor
    '''
    def __init__ (self):
	#here i make an istance of interfaceROS in order to communicate and save this PattiCore istance
	self.interfaceROS = InterfaceROS(self)

    '''
    This function is the callback when received a message
    '''
    def getValues(self, msg):
       	#print in loginfo the interaction1 and interaction1 and others message field in order to debug 	
    	for i in range (2):
    		rospy.loginfo("interaction%d: %f " % (i, msg.interaction[i]))
    		rospy.loginfo ("emptiness%d: %f" % (i, msg.emptiness[i]))
    	#this is out of the loop because emptiness dimension is 3 but interaction dimension is 2
    	rospy.loginfo ("emptiness%s: %f" % ('2', msg.emptiness[2]))
    
    	#Here i copy message values in the array and then call interfaceROS function to elaborate the data
    	sensorInteraction = []
    	sensorEmptiness = []
    	for i in range (2) :
    		sensorInteraction.append(msg.interaction[i])
   	for j in range (3) :
    		sensorEmptiness.append(msg.emptiness[j])
    	#send those values to interface ros in order to be elaborate
    	#interfaceROS = InterfaceROS()
    	self.interfaceROS.sendSensorValues(sensorInteraction[0], sensorInteraction[1], sensorEmptiness[0], sensorEmptiness[1], sensorEmptiness[2])

    '''
    Here i set up listeners to topic
    '''    
    def pattiCore(self):

    	#will initialize this node listener
    	rospy.init_node('pattiCore', anonymous=True)
    	#here I subscribe to sensorValuesTopic, type of message is SensorValues and getValues is the function I use 
    	#after receive a message
    	rospy.Subscriber("sensorsValuesTopic", SensorValues, self.getValues)

    	# rospy spin keeps python to exit 
    	rospy.spin()
        

    '''
    This function will elaborate and send a message thought emotional state topic
    '''
    def sendState(self, RGBsettings,  coverSettings, movementSettings):
	pub = rospy.Publisher('emotialStateSettingsTopic', EmotionalStateSettings, queue_size = 5)
	#create the message and save the values in it
	emotionalStateSettings = EmotionalStateSettings ()
	emotionalStateSettings.RGB = RGBsettings
	emotionalStateSettings.cover = coverSettings
	emotionalStateSettings.movement = movementSettings
	#publish the message
	rospy.loginfo(emotionalStateSettings)
        pub.publish(emotionalStateSettings)
	

'''
Uses this main for debugg
'''
if __name__ == '__main__':
    pattiCore = PattiCore()
    pattiCore.pattiCore()

