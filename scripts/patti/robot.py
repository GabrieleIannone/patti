#!/usr/bin/env python
import rospy
from movement import Movement
from std_msgs.msg import String
from random import uniform
from patti_project.msg import SensorValues
from patti_project.msg import Proximity

'''
Use this function to manage hardware robot part with ros
'''
def robot():
    #'sensorValues' is the topic name where my node is publishing, SensorValue is the type of message. queue_size is the max message queue
    pub = rospy.Publisher('sensorsValuesTopic', SensorValues, queue_size = 5)
    #robot is the name of the node, anonymous = true ensures that this is an unique name adding some random int 
    #after the name
    rospy.init_node('robot', anonymous=True)
    #object rate Time = 1/frequency
    print ("test")
    rate = rospy.Rate(0.2) # 5 seconds loop

    #standard loop checking if rospy-is.shutdown flag and then doing work. 
    while not rospy.is_shutdown():
        print ("loop")
	'''
	Set up the message with sensors values to get emotional state
	'''
	#Here i create a sensor value type message
	sensorValuesMessage = SensorValues()
	#fill the message with random float values
        sensorValuesMessage.interaction = [(uniform (0,1000)), (uniform (0,1000))]
	sensorValuesMessage.emptiness = [uniform (0,500), uniform (0,500), uniform (0,500)]
	#publish the message in loginfo and in the topic
        rospy.loginfo(sensorValuesMessage)
        pub.publish(sensorValuesMessage)

        rate.sleep()	

'''
main function
'''
if __name__ == '__main__':
    try:
        robot()
    except rospy.ROSInterruptException:
        pass
