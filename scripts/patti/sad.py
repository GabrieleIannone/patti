#!/usr/bin/env python

from state import State

class Sad(State):
    '''
    This class is for Sad state
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.name = 'Sad'
	#those are settings for robot when it is happy
        self.RGBsettings = 'RGB colour: (0,0,255) blue '
	self.coverSettings = 'Closed'
	self.movementSettings = 'Keep going slowly'
 
    '''
    @see state
    '''
    def getName(self):
        return self.name + ' :,('
    

'''
Use this with debug
'''
if __name__ == '__main__':
    sadState = Sad()
    print('Hey! I am ' + sadState.__str__() + ' :,(')
