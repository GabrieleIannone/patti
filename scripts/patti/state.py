#!/usr/bin/env python

from abc import abstractmethod

class State(object):
    '''
    This class is an "abstract class", a father for all state classes
    '''

    '''
    This method will return the state name 
    '''
    @abstractmethod
    def getName(self):
        pass
    
    '''
    Use this when you want to write down the state
    '''
    def __str__(self):
        return self.getName()
