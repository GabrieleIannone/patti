#!/usr/bin/env python3

import json

data = {}
data['transition'] = []

'''
state is the name of the state corresponding to sensors values
s1 is the digital value of the sensor that tell us the grade of patti fullness (empty =0, middle full =1, full =2)
s2 is the digital value of the sensor that tell us the interaction with patti (little interaction =0, much interaction =2)
'''
data['transition'].append ( {
	'state' : 'Sad',
	'module' : 'sad',
	's1' : '0',
	's2' : '0'
})
data['transition'].append ( {
	'state' : 'Curious',
	'module' : 'curious',
	's1' : '0',
	's2' : '2'
})
data['transition'].append ( {
	'state' : 'Bored',
	'module' : 'bored',
	's1' : '1',
	's2' : '0'
})
data['transition'].append ( {
	'state' : 'Happy',
	'module' : 'happy',
	's1' : '1',
	's2' : '2'
})
data['transition'].append ( {
	'state' : 'Angry',
	'module' : 'angry',
	's1' : '2',
	's2' : '0'
})
data['transition'].append ( {
	'state' : 'Embarassed',
	'module' : 'embarassed',
	's1' : '2',
	's2' : '2'
})

#write on the file
with open('transition.txt', 'w') as outfile:
	json.dump(data, outfile)
